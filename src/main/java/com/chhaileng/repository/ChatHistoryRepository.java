package com.chhaileng.repository;

import com.chhaileng.repository.provider.ChatProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatHistoryRepository {
    @InsertProvider(type = ChatProvider.class, method = "chatHistory")
    boolean chatHistory();
}
