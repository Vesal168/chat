package com.chhaileng.repository.provider;
import org.apache.ibatis.jdbc.SQL;

public class ChatProvider {
    public String ChatHistory(){
        return new SQL(){{
            INSERT_INTO("dp_chat_history");
            VALUES("user_id, name, message", "#{user_id}, #{name}, #{message}");
        }}.toString();
    }
}
